# spring-boot-demo

#### 介绍
spring boot测试项目

- 支持post请求
- 支持postJson数据
- 支持上传文件
- 支持文件下载
- 支持全局异常处理
- 支持全局token校验处理
- 支持zuul限流控制
- 支持mybaits数据库操作
- 支持redis操作
