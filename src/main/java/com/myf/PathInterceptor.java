package com.myf;

import com.myf.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * 请求拦截器
 */
@Component
public class PathInterceptor implements HandlerInterceptor {

    protected final Logger logger = Utils.getLogger(this.getClass());

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("preHandle");
        String sesssionID = request.getSession().getId();
        logger.info("preHandle="+sesssionID);
        System.out.println(request.getParameterMap());
        System.out.println(request.getRequestedSessionId());
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("afterCompletion");
        logger.info("afterCompletion"+request.getSession().getId());
    }


}
