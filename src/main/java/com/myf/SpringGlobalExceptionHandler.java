package com.myf;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class SpringGlobalExceptionHandler {

    @ExceptionHandler(value =Exception.class)
    @ResponseBody
    public Object exceptionHandler(Exception e){
        System.out.println("未知异常！原因是:"+e);
        Map<String,String> map = new HashMap<>();
        map.put("error",e.getMessage());
        return map;
    }

}
