package com.myf.service;

import com.myf.dao.AccountDao;
import com.myf.entity.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AccountService {

    @Autowired
    private AccountDao accountDao;

    /**
     * 转钱给outName
     * @param inName
     * @param outName
     * @param money
     */
    public List<Account> transferMoney(String inName, String outName, double money){
        accountDao.outMoney(outName,money);
        //int i=1/0;
        accountDao.inMoney(inName,money);
        List<String> names = new ArrayList<>();
        names.add(inName);
        names.add(outName);
        List<Account> accounts = accountDao.findAccountByNames(names);
        return  accounts;
    }

    /**
     * 插入新账户
     * @param name
     * @param initMoney
     * @return
     */
    public Account addAccount(String name,double initMoney){
        Account account = accountDao.findAccountByName(name);
        if(account==null){
            accountDao.addAccount(name,initMoney);
            account = accountDao.findAccountByName(name);
        }else{
            new RuntimeException("姓名已经存在");
        }
        return account;
    }
}
