package com.myf.controller;

import com.myf.dao.StudentDao;
import com.myf.entity.Account;
import com.myf.entity.Student;
import com.myf.service.AccountService;
import com.myf.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class HelloController {

    @Autowired
    private StudentDao studentDao;

    @Autowired
    private AccountService accountService;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;


    protected final Logger logger = Utils.getLogger(this.getClass());

    @RequestMapping("/redis")
    @ResponseBody
    public Object redis(){
        ValueOperations<String, String> operations = redisTemplate.opsForValue();
        String val = operations.get("myf");
        Map<String,String> res = new HashMap<>();
        res.put("val",val);

        ValueOperations<String, String> stringStringValueOperations = stringRedisTemplate.opsForValue();
        stringStringValueOperations.set("myf","abc-myf");
        String newVal = stringStringValueOperations.get("myf");
        res.put("newVal",newVal);

        return res;
    }

    @RequestMapping("/index")
    public String sayHello(){
        return "index";
    }

    @RequestMapping("/login")
    public String login(Model model){
        model.addAttribute("username","输入用户名:xx-myfdd");
        model.addAttribute("password","输入密码:");
        return "login";
    }

    @RequestMapping("/json")
    @ResponseBody
    public List<String> getUsers(){
        List<String> list = new ArrayList<>();
        list.add("abc");
        return list;
    }

    @ResponseBody
    @RequestMapping("/students")
    public List<Student> getStudents(){
        List<Student> students = studentDao.findStudents();
        return students;
    }

    @ResponseBody
    @RequestMapping("/student")
    public Student getStudent(){
        int id= 1;
        Student student = studentDao.findStudent(id);
        logger.info(student.toString());
        return student;
    }

    @ResponseBody
    @RequestMapping("/transferMoney")
    public List<Account> transferMoney(){
        String inName = "tom";
        String outName = "myf";
        double money = 500F;
        List<Account> accounts = accountService.transferMoney(inName,outName,money);
        return  accounts;
    }

    @ResponseBody
    @RequestMapping("/addAccount")
    public Account addNewAccount(@RequestParam(name = "name") String name,@RequestParam(name = "money") Double money){
        System.out.println("name="+name);
        System.out.println("money="+money);
       return accountService.addAccount(name,money);
    }


    @ResponseBody
    @PostMapping("/requestPostJson")
    public Object requestPostJson(@RequestBody Map<String, String> person){
        System.out.println(person);
        return person;
    }


    /**
     * post一个json文件
     * @param person
     * @return
     */
    @ResponseBody
    @PostMapping("/requestAccountJson")
    public Object requestPostJson(@RequestBody Account person){
        System.out.println(person);
        return person;
    }


    /**
     * post参数
     * @param person
     * @return
     */
    @ResponseBody
    @PostMapping("/requestAccountData")
    public Object requestPostData(Account person){
        System.out.println(person);
        return person;
    }

    /**
     * 获取header的值
     * @param request
     * @return
     */
    @ResponseBody
    @PostMapping("/requestHeaderParam")
    public Object requestHeaderParam(HttpServletRequest request){
        String token = request.getHeader("token");
        Map<String,String> map = new HashMap<>();
        map.put("token",token);
        return map;
    }

    /**
     * 上传单个文件
     * @param file
     * @return
     * @throws IOException
     */
    @ResponseBody
    @PostMapping("/uploadFile")
    public Object requestFile(@RequestParam("file")MultipartFile file) throws IOException {
        if(file.isEmpty()){
           new RuntimeException("请上传文件");
        }
        Map<String,String> map = new HashMap<>();
        String fileName = file.getOriginalFilename();
        String suffix = fileName.substring(fileName.lastIndexOf("."));
        String filePath = "/Users/mac/Downloads/springBootUploads/";
        String path = filePath+fileName;
        File dest = new File(path);
        if(!dest.getParentFile().exists()){
            dest.getParentFile().mkdirs();
        }
        file.transferTo(dest);

        map.put("filename",fileName);
        map.put("suffix",suffix);
        map.put("newFile",path);
        return map;
    }

    /**
     * 下载文件
     * @param request
     * @param response
     * @throws IOException
     */
    @GetMapping("/downloadFile")
    public void downloadFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String fileName = "hym.jpeg";
        File file = new File("/Users/mac/Downloads/springBootUploads/胡艳敏.jpeg");
        response.setContentType("application/force-download");
        response.addHeader("Content-Disposition","attachment;fileName="+fileName);
        byte[] buffer = new byte[1024];
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try{
            fis = new FileInputStream(file);
            bis = new BufferedInputStream(fis);
            OutputStream os = response.getOutputStream();
            int i= bis.read(buffer);
            while (i!=-1){
                os.write(buffer,0,i);
                i=bis.read(buffer);
            }

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            if(bis!=null){
                bis.close();
            }
            if(fis!=null){
                fis.close();
            }
        }
    }
}
