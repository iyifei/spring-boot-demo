package com.myf.dao;

import com.myf.entity.Student;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StudentDao {

    @Results(id = "studentMap" , value = {
            @Result(property = "createTime",column = "create_time")
    })
    @Select("select * from student")
    List<Student> findStudents();

    @Select("select * from student where id=#{id}")
    @ResultMap("studentMap")
    Student findStudent(Integer id);
}
