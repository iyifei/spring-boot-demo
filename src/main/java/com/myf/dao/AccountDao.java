package com.myf.dao;

import com.myf.entity.Account;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountDao {

    @Update("update account set money=money+#{money} where name=#{name}")
    public void inMoney(String name,double money);

    @Update("update account set money=money-#{money} where name=#{name}")
    public void outMoney(String name,double money);

    @Select(
            "<script> " +
            "select * from account where id in " +
            "<foreach item='item' index='index' collection='ids' open='(' separator=',' close=')'> " +
            "   #{item} " +
            "</foreach>" +
            "</script> "
    )
    public List<Account> findAccountByIds(List<Integer> ids);


    @Select(
            "<script> " +
                    "select * from account where name in " +
                    "<foreach item='item' index='index' collection='names' open='(' separator=',' close=')'> " +
                    "   #{item} " +
                    "</foreach>" +
                    "</script> "
    )
    public List<Account> findAccountByNames(List<String> names);


    /**
     * 插入一个账号，返回账号id
     * @param name
     * @param money
     * @return
     */
    @Update("insert into account(name,money) values(#{name},#{money})")
    public int addAccount(String name,double money);

    /**
     * 查询一条记录
     * @param name
     * @return
     */
    @Select("select * from account where name=#{name}")
    public Account findAccountByName(String name);

}
