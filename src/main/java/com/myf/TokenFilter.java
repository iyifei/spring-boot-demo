package com.myf;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class TokenFilter extends ZuulFilter {

    /**
     * 拦截类型
     * pre 之前拦截
     * route 路由时拦截
     * post 请求拦截
     * error 出错拦截
     * @return
     */
    @Override
    public String filterType() {
        return "pre";
    }

    /**
     * 拦截的值约小约靠前
     * @return
     */
    @Override
    public int filterOrder() {
        return 0;
    }

    /**
     * 该拦截器是否生效
     * @return
     */
    @Override
    public boolean shouldFilter() {
        return true;
    }

    /**
     * 拦截后做啥事情
     * @return
     * @throws ZuulException
     */
    @Override
    public Object run() throws ZuulException {
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        //从header取出token
        String token = request.getHeader("token");
        System.out.println("token:"+token);
        if(StringUtils.isEmpty(token)){
            currentContext.setResponseBody("token is null");
            currentContext.setResponseStatusCode(401);
            currentContext.setSendZuulResponse(false);
            return null;
        }
        currentContext.setSendZuulResponse(true);
        return null;
    }
}
