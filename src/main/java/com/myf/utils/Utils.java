package com.myf.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Utils {

    static Map<String,Logger> logs = new HashMap<>();

    public static Logger getLogger(Class<?> _class){
        Logger logger = null;
        if(logs.containsKey(_class.toString())){
            logger = logs.get(_class.toString());
        }else{
            String uuid = UUID.randomUUID().toString();
            logger =  LoggerFactory.getLogger(_class.toString()+"-"+uuid);
            logs.put(_class.toString(),logger);
        }
        return logger;
    }

}
